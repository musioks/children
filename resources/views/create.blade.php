<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>NEEDY CHILDREN AND SPONSOR LINKING PLATFORM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('bootstrap/css/font-awesome.min.css')}}">
 
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{asset('plugins/iCheck/square/blue.css')}}">
    <style type="text/css">
        input[type="email"],input[type="password"],input[type="checkbox"],button[type="submit"]{
            border-radius:5px;
        }
        .jumbotron{
            background: #337ab7;
            color: #FFFFFF;
        }
    </style>
       <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
</head>
<body class="hold-transition login-page">

<div class="login-box" style="margin-top: 10px;">
    <div class="login-logo">
        <a href="{{url('/create')}}"><b> Create Admin Account</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Register below</p>

        <form action="{{url('/create')}}" method="post">
            {{csrf_field()}}
            <div class="form-group">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        {{Session::get('error')}}
                    </div>
                    @endif
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                   
            </div>
            <div class="form-group has-feedback {{$errors->has('name') ? 'has-error':''}}">
                <input type="text" class="form-control" name="name" placeholder="Full Name" autofocus="">
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                @if($errors->has('name'))
<span class="help-block">{{$errors->first('name')}}</span>
@endif
            </div>
            <div class="form-group has-feedback {{$errors->has('email') ? 'has-error':''}}">
                <input type="email" class="form-control" name="email" placeholder="Email address" autofocus="">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if($errors->has('email'))
<span class="help-block">{{$errors->first('email')}}</span>
@endif
            </div>
            <div class="form-group has-feedback {{$errors->has('password') ? 'has-error':''}}">
                <input type="password" class="form-control" name="password" placeholder="Password" autofocus="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if($errors->has('password'))
<span class="help-block">{{$errors->first('password')}}</span>
@endif
            </div>
                <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password_confirmation" placeholder="Password Confirmation" autofocus="">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

        <div class="social-auth-links text-center">
        <hr>
<button type="submit"  class="btn btn-block  btn-facebook  btn-flat"><i class="fa fa-rocket"></i> Sign up </button>

        </div>
        <a href="{{url('/')}}" class="btn btn-warning btn-block">Back to Home</a>
                </form>

        <!-- /.social-auth-links -->
        <hr>


    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<!-- iCheck -->
<script src="{{asset('plugins/iCheck/icheck.min.js')}}"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>