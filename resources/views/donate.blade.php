<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>NEEDY CHILDREN AND SPONSOR LINKING PLATFORM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('bootstrap/css/font-awesome.min.css')}}">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<!-- jvectormap -->
 <!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
    <style type="text/css">
        input[type="email"],input[type="password"],input[type="checkbox"],button[type="submit"]{
            border-radius:5px;
        }
        .jumbotron{
            background: #337ab7;
            color: #FFFFFF;
        }
    </style>
       <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
</head>
<body>
<nav class="navbar navbar-inverse" style="border-radius: 0px;">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{url('/')}}">
					NEEDY CHILDREN AND SPONSOR LINKING PLATFORM
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						
						<li>
							<a  href="{{url('/support')}}">Sponsor a Needy Child</a>
						</li>
						<li>
							<a  href="{{url('/donate')}}">Donate</a>
						</li>
					</ul>
						<ul class="nav navbar-nav navbar-right">
						
						<li>
							<a class="" href="{{url('/login')}}">Sign In</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<div class="container">
		<div class="jumbotron">
		<h3 class="text-center">Donation Information</h3>
		</div>
         <div class="box">
                <div class="box-header">
                    <h3 class="box-title text-success">Donate to a Needy Child </h3>
                </div>
                            <div class="box-body">
<form  enctype="multipart/form-data" method="post" action="{{url('/donate')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('name') ? 'has-error':''}}">
                        <label>Full Name *</label>
                        <input type="text" name="name" class="form-control" placeholder="eg. John Doe">
                        @if($errors->has('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('email') ? 'has-error':''}}">
                        <label> Email*</label>
                        <input type="email" name="email" class="form-control" placeholder="Email Address">
                        @if($errors->has('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                  <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('city') ? 'has-error':''}}">
                        <label> City/Town*</label>
                        <input type="text" name="city" class="form-control" placeholder="City">
                        @if($errors->has('city'))
              <span class="help-block">{{$errors->first('city')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 
                </div><!--end row--> 
                <div class="row">
                       <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('address') ? 'has-error':''}}">
                        <label>Address *</label>
                        <input type="text" name="address" class="form-control" placeholder="Address">
                        @if($errors->has('address'))
              <span class="help-block">{{$errors->first('address')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('email') ? 'has-error':''}}">
                        <label> Donation Amount*</label>
                        <input type="number" name="amount" class="form-control" placeholder="eg. KES 4,500">
                        @if($errors->has('amount'))
              <span class="help-block">{{$errors->first('amount')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                  <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('payment_type') ? 'has-error':''}}">
                        <label>Payment Type</label>
                        <div class="radio">
  <label>
    <input type="radio" name="payment_type" id="optionsRadios1" value="Paypal" checked>
    Paypal
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="payment_type" id="optionsRadios2" value="MPESA">
Offline Payment(M-Pesa/EFT) </label>
</div>
                        @if($errors->has('payment_type'))
              <span class="help-block">{{$errors->first('payment_type')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 
                </div><!--end row-->  

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-success" >Donate Now</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
            <hr>
             <div class="row">
                <div class="col-sm-12">
                <h4 class="text-success">LIPA NA MPESA</h4>
                <p>

                  M-PESA PAYBILL NO. 123456<br>
· You need to be an M-PESA registered customer.<br>
· Go to M-PESA on your phone menu<br>
· Select payment services/Lipa na M-PESA<br>
· Select Pay Bill<br>
· Enter business No. 123456<br>
· Enter the ‘Account Number' eg ABC Bookshop, Julius Thuku ,X company etc<br>
· Enter the amount you wish to pay(between Ksh 10-Ksh 70,000)<br>
· Enter your M-PESA PIN<br>
· Confirm details are correct and press ok<br>

This is a Business Bouquet Pay Bill where a customer is charged for the amount sent as per Safaricom's tariff guide.<br>
For clarifications you may contact the undersigned.<br>
NEEDY CHILDREN AND SPONSOR LINKING PLATFORM ACCOUNTS<br>
0795353222
                </p>
                </div> 
                </div>
	  
                    </div>
                    </div>
		</div>


<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/app.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('plugins/jquery.PrintArea.js')}}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard2.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script type="text/javascript">
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
       //Initialize Select2 Elements
$(".select2").select2();
      //Date picker
$('#datepicker').datepicker({
  autoclose: true
});
  $('#datepicker1').datepicker({
  autoclose: true
});
</script>
</body>
</html>