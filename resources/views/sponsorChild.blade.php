<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>NEEDY CHILDREN AND SPONSOR LINKING PLATFORM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('bootstrap/css/font-awesome.min.css')}}">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<!-- jvectormap -->
 <!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
    <style type="text/css">
        input[type="email"],input[type="password"],input[type="checkbox"],button[type="submit"]{
            border-radius:5px;
        }
        .jumbotron{
            background: #337ab7;
            color: #FFFFFF;
        }
    </style>
       <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
</head>
<body>
<nav class="navbar navbar-inverse" style="border-radius: 0px;">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{url('/')}}">
					NEEDY CHILDREN AND SPONSOR LINKING PLATFORM
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						
						<li>
							<a  href="{{url('/support')}}">Sponsor a Needy Child</a>
						</li>
						<li>
							<a  href="{{url('/donate')}}">Donate</a>
						</li>
					</ul>
						<ul class="nav navbar-nav navbar-right">
						
						<li>
							<a class="" href="{{url('/login')}}">Sign In</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<div class="container">
		<div class="jumbotron">
		<h3 class="text-center">Change a Child's Life by Sponsoring!</h3>
		</div>
         <div class="box">
                <div class="box-header">
                    <p class="box-title text-success">For $30 or more a month, you can help provide a child with their daily needs, and when they are ready, an education.  Sponsoring a child lets him or her know there is someone who cares!
 </p>
 <hr>
 <p class="text-justify">
   To sponsor a child or children, fill the sponsorship form below and submit. 
(After receiving your completed form, we will contact you with the details of your sponsored child and gather information on your method of payment.)
 </p>
                </div>
                            <div class="box-body">
                            <fieldset>
                            <legend>Sponsorship Form</legend>
<form  enctype="multipart/form-data" method="post" action="{{url('/sponsorChild/'.$kids->id)}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group ">
                        <label>Child's Name *</label>
                        <input type="text" name="" class="form-control" value="{{$kids->lname}} {{$kids->lname}}">
                        <input type="hidden" name="kid_id" class="form-control" value="{{$kids->id}}">
                       
                    </div>
                <!-- /.form-group -->
                </div>
                  <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('name') ? 'has-error':''}}">
                        <label>Sponsor Name *</label>
                        <input type="text" name="name" class="form-control" placeholder="Enter Name">
                            @if($errors->has('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
                       @endif                   
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-3">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('email') ? 'has-error':''}}">
                        <label> Email*</label>
                        <input type="email" name="email" class="form-control" placeholder="Email Address">
                        @if($errors->has('email'))
              <span class="help-block">{{$errors->first('email')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                  <div class="col-sm-3">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('town') ? 'has-error':''}}">
                        <label> City/Town*</label>
                        <input type="text" name="town" class="form-control" placeholder="town">
                        @if($errors->has('town'))
              <span class="help-block">{{$errors->first('town')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 
                </div><!--end row--> 
                <div class="row">
                       <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('address') ? 'has-error':''}}">
                        <label>Address *</label>
                        <input type="text" name="address" class="form-control" placeholder="Address">
                        @if($errors->has('address'))
              <span class="help-block">{{$errors->first('address')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                 <div class="col-md-3">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('sponsor_period') ? 'has-error':''}}">
                        <label>Period of Sponsor *</label>
                        <select name="sponsor_period" class="form-control">
                        <option value="">----Select Period of Sponsor----</option>
                        <option value="3 Months">3 Months</option>
                        <option value="6 Months">6 Months</option>
                        <option value="1 Year"> 1 Year</option>
                        <option value="2 Years"> 2 Years</option>
                        <option value="3 Years"> 3 Years</option>
                        <option value="4 Years"> 4 Years</option>
                        </select>
                        @if($errors->has('sponsor_period'))
              <span class="help-block">{{$errors->first('sponsor_period')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-3">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('email') ? 'has-error':''}}">
                        <label> Support Amount*</label>
                        <input type="number" name="amount" class="form-control" placeholder="eg. KES 4,500">
                        @if($errors->has('amount'))
              <span class="help-block">{{$errors->first('amount')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                  <div class="col-sm-3">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('payment') ? 'has-error':''}}">
                        <label>Payment Type</label>
                        <div class="radio">
  <label>
    <input type="radio" name="payment" id="optionsRadios1" value="Paypal" checked>
    Paypal
  </label>
</div>
<div class="radio">
  <label>
    <input type="radio" name="payment" id="optionsRadios2" value="MPESA">
Offline Payment(M-Pesa/EFT) </label>
</div>
                        @if($errors->has('payment'))
              <span class="help-block">{{$errors->first('payment')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 
                </div><!--end row-->  

                <div class="row">                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-success" >Submit Now</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
            </fieldset>
            <hr>
         
	  
                    </div>
                    </div>
		</div>


<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/app.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('plugins/jquery.PrintArea.js')}}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard2.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script type="text/javascript">
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
       //Initialize Select2 Elements
$(".select2").select2();
      //Date picker
$('#datepicker').datepicker({
  autoclose: true
});
  $('#datepicker1').datepicker({
  autoclose: true
});
</script>
</body>
</html>