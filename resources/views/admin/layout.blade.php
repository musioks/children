<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Street Children and Sponsors Linking Information System | @yield('title')</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.6 -->
<link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('bootstrap/css/font-awesome.min.css')}}">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<!-- jvectormap -->
 <!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
<style type="text/css">

.d_image{
        height:100px;
 width:200px;
    }
</style>
@yield('styles')

</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">

<header class="main-header">

    <!-- Logo -->
    <a href="{{route('admin.index')}}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>SCSLIS</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Control Panel</b></span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{url('users/'.Sentinel::getUser()->avatar)}}" class="user-image" alt="User Image">
                        <span class="hidden-xs">    {{Sentinel::getUser()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{url('users/'.Sentinel::getUser()->avatar)}}" class="img-circle" alt="User Image">

                            <p>
                                {{Sentinel::getUser()->name}}
                                <small>{{Sentinel::getUser()->email}}</small>
                            </p>
                        </li>

                        <li class="user-footer">
                                <a href="{{route('profile')}}" class="btn btn-default"><i class="fa fa-user"></i>Profile</a>
                        </li>
                        <li class="user-footer">
                            <a href="{{route('cpassword')}}" class="btn btn-info " style="color:#FFF;"><i class="fa fa-lock"></i>Change Password</a>
                        </li>
                        <li class="user-footer">
                        <a  href="{{ route('signout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();" class="btn btn-danger " style="color:#FFF;"><i class="fa fa-power-off"></i>Sign out</a>
                         <form id="logout-form" action="{{ route('signout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                             </form>
                        </li>

                    </ul>
                </li>

            </ul>
        </div>

    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{url('users/'.Sentinel::getUser()->avatar)}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Sentinel::getUser()->name}}</p>
            </div>
        </div>
      
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN MENU</li>
            <li class="active">
                <a href="{{route('admin.index')}}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>

            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>Needy Children</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('addkid')}}"><i class="fa fa-plus"></i>Add New</a></li>
                    <li><a href="{{route('children')}}"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-home"></i><span>Rehab Centers</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{route('addrehab')}}"><i class="fa fa-plus"></i>Add Rehab Center</a></li>
        <li><a href="{{route('childrenRehab')}}"><i class="fa fa-group"></i>Children Per Rehab</a></li>
        <li><a href="{{route('rehabs')}}"><i class="fa fa-circle-o"></i> All Rehabs</a></li>
                </ul>
            </li>
             <li class="treeview">
                <a href="#">
                    <i class="fa fa-money"></i><span>Donations</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
        <li><a href="{{route('donations')}}"><i class="fa fa-circle-o"></i> View Donations</a></li>
                </ul>
            </li>
          <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i><span>Sponsorships</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
        <li><a href="{{route('sponsors')}}"><i class="fa fa-circle-o"></i> View All</a></li>
                </ul>
            </li>
              <li class="treeview">
                <a href="#">
                    <i class="fa fa-th"></i><span>Manage Users</span>
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
                </a>
                <ul class="treeview-menu">
        <li><a href="{{route('getUsers')}}"><i class="fa fa-circle-o"></i> All users</a></li>
                </ul>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            @yield('page')
        </h1>

    </section>

    <!-- Main content -->
    <section class="content">
        @yield('content')



    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<footer class="main-footer">
    <div class="pull-right hidden-xs">
    </div>
    <strong>Copyright &copy; 2017 <a href="{{route('admin.index')}}">NEEDY CHILDREN AND SPONSOR LINKING PLATFORM</a>.</strong> All rights
    reserved.
</footer>




</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/app.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('plugins/jquery.PrintArea.js')}}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard2.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script type="text/javascript">
    $('#example1').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false
    });
       //Initialize Select2 Elements
$(".select2").select2();
      //Date picker
$('#datepicker').datepicker({
  autoclose: true
});
  $('#datepicker1').datepicker({
  autoclose: true
});
</script>
@yield('scripts')
</body>
</html>
