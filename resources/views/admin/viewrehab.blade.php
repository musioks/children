@extends('admin.layout')
@section('title') View Rehabilitation Center @stop
@section('content')
        <!-- SELECT2 EXAMPLE -->
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">View/Update Rehabilitation Center</h3>
    </div>
    <!-- /.box-header -->
    
            <form  method="post" action="{{url('/viewrehab/'.$rehabs->id)}}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group ">
                        <label>Rehab Center Name *</label>
                        <input type="text" name="name" class="form-control" value="{{$rehabs->name}}">
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group">
                        <label> Where is it Located?*</label>
                        <input type="text" name="location" class="form-control" value="{{$rehabs->location}}">
                     
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 
                </div><!--end row-->                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-warning" >Update</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
   

</div>
<!-- /.box -->
@stop
