@extends('admin.layout')
@section('title') View Children per Rehabilitation center @stop
@section('page')View Children per Rehabilitation center @stop
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
        <div class="box-header">
        <div class="row">
        <form  method="POST" action="/searchScope" role="search">
                {{csrf_field()}}
              <div class="col-sm-6">
                 <div class="form-group">
                        <label>Rehabilitation Center</label>
                        <select name="rehab_id" class="form-control select2">
                           <option value="">-----Select Rehabilitation Center-----</option>
                        @foreach($rehabs as $rehabs)
                            <option value="{{$rehabs->id}}">{{$rehabs->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group pull-right">
                    <button type="submit" class="btn btn-warning">View Children</button>
                    </div> 
                    </div>              
            </form>
            </div>
                </div>

                <!-- /.box-header -->
                <div class="box-body" style="width: 100%">
                  @if(isset($details))
                   
                <form action="{{url('/rehabReport')}}" method="get" class="pull-right">
                {{csrf_field()}}
         <input type="hidden" name="rehab_id" value="
@foreach($details as $kids)
@if($details->first() == $kids)
         {{$kids->rehab_id}}
         @endif
@endforeach
         ">
<button type="submit"  class="btn bg-navy text-center">
        <i class="fa fa-print"></i>Generate PDF</button>
        </form>
        
                  <h4>&nbsp;</h4>
                    <table id="example1" class="table table-bordered table-striped table-condensed table-hover">
                         <thead>
                        <tr>
                             <th>Photo</th>
                            <th>Child Name</th>
                            <th>Age</th>
                            <th>National ID</th>
                            <th>Gender</th>
                            <th>Education Progress</th>
                            <th>Residence</th>
                            <th>County</th>
                            <th>Action</th>

                           
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($details as $kids)

                            <tr>
                    <td style="width:200px;"><img src="{{url('kids/'.$kids->img)}}" class="img-responsive"></td>

                                <td>{{$kids->fname}}{{" "}}{{$kids->lname}}</td>
                                <td>
                                @php 
                               
                                  $d=date("m/d/Y");
                                  $d1=$kids->dob;
                                   $date1=new DateTime($d);
                                $date2=new DateTime($d1);
                                $diff = $date1->diff($date2);
                                echo $diff->y;@endphp</td>
                                 <td>{{$kids->nid}}</td>
                                <td>{{$kids->gender}}</td>
                                <td>{{$kids->education}}</td>
                                <td>{{$kids->residence}}</td>
                                <td>{{$kids->county}}</td>
                                 <td style="width:180px;"><a href="{{route('deleteKid',['kids'=>$kids->id])}}"  onclick="return confirm('Do you want to Delete ?')" class="btn btn-danger"><i class="fa fa-fw fa-remove"></i>Delete </a>
                                    <a href="{{url('/viewkid/'.$kids->id)}}" class="btn btn-success"><i class="fa fa-fw fa-eye"></i>View</a></td>
                                
        
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                    @endif
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop