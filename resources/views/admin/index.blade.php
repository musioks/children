@extends('admin.layout')
@section('title') Control Panel @stop
@section('content')

<div class="row">

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                         <a href="">
                        <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text">Needy Children</span>
                            <span class="info-box-number">{{count($kids)}}</span>
                        </div>
                        </a>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4 col-sm-6 col-xs-12">
                         <div class="info-box">
                      <a href="">
                        <span class="info-box-icon bg-red"><i class="fa fa-user"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Users</span>
                            <span class="info-box-number">{{count($users)}}</span>
                        </div>
                        </a>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->

                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-4 col-sm-6 col-xs-12">
                    <div class="info-box">
                    <a href="">
                        <span class="info-box-icon bg-green"><i class="fa fa-home"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Rehabilation Centers</span>
                            <span class="info-box-number">{{count($rehabs)}}</span>
                        </div>
                        </a>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
            
            </div>
            <!-- /.row -->
@stop
