@extends('admin.layout')
@section('title') Needy Children @stop
@section('page') All Needy Children @stop
@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Needy Children</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                    <a href="{{url('/kidsPDF')}}"  class=" btn bg-navy pull-right">
        <i class="fa fa-print"></i> Generate PDF</a>
        <h4>&nbsp;</h4>
                    <table id="example1" class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                             <th>Photo</th>
                            <th>Child Name</th>
                            <th>Age</th>
                            <th>National ID</th>
                            <th>Gender</th>
                            <th>Education Progress</th>
                            <th>Residence</th>
                            <th>County</th>
                            <th>Rehab. Center</th>
                            <th>Action</th>

                           
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kids as $kids)

                            <tr>
                    <td style="width:200px;"><img src="{{asset('kids/'.$kids->img)}}" class="img-responsive"></td>

                                <td>{{$kids->fname}}{{" "}}{{$kids->lname}}</td>
                                <td>
                                @php 
                               
                                  $d=date("m/d/Y");
                                  $d1=$kids->dob;
                                   $date1=new DateTime($d);
                                $date2=new DateTime($d1);
                                $diff = $date1->diff($date2);
                                echo $diff->y;@endphp</td>
                                 <td>{{$kids->nid}}</td>
                                <td>{{$kids->gender}}</td>
                                <td>{{$kids->education}}</td>
                                <td>{{$kids->residence}}</td>
                                <td>{{$kids->county}}</td>
                                <td>{{$kids->name}}</td>
                                 <td style="width:180px;"><a href="{{route('deleteKid',['kids'=>$kids->id])}}"  onclick="return confirm('Do you want to Delete ?')" class="btn btn-danger"><i class="fa fa-fw fa-remove"></i>Delete </a>
                                    <a href="{{url('/viewkid/'.$kids->id)}}" class="btn btn-success"><i class="fa fa-fw fa-eye"></i>View</a></td>
                                
        
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                 
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop
