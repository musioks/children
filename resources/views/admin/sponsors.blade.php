@extends('admin.layout')
@section('title') Sponsorships  @stop
@section('page') Sponsorships  @stop
@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box box-danger">
                <div class="box-header">
                    <h3 class="box-title">List Showing All Sponsorships.</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                      <a href="{{url('/sponsorsPDF')}}"  class=" btn bg-success pull-right">
        <i class="fa fa-print"></i> Generate PDF</a>
        <h4>&nbsp;</h4>
    <table id="example1" class="table table-bordered">
                        <thead>
                        <tr>
                        
                            <th>Name of Child</th>
                            <th>Name of Sponsor</th>
                            <th>Email</th>
                            <th>City/Town</th>
                            <th>Address</th>
                            <th>Period of Sponsor</th>
                            <th>Type of Payment</th>
                            <th>Date of Sponsorship</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($sponsors as $sponsors)

                            <tr>
                    
                            
                                <td>{{$sponsors->fname}} {{$sponsors->lname}}</td>
                                <td>{{$sponsors->name}}</td>
                                <td>{{$sponsors->email}}</td>
                                <td>{{$sponsors->town}}</td>
                                <td>{{$sponsors->address}}</td>
                                <td>{{$sponsors->sponsor_period}}</td>
                                <td>{{$sponsors->payment}}</td>
                                <td>{{ date('F d, Y', strtotime($sponsors->created_at)) }}</td>
                                <td>{{$sponsors->amount}}</td>
                                
                                
        
                            </tr>
                        @endforeach

                        </tbody>
                        <tr>
                            <td colspan="8"><strong>Total</strong></td>
                            <td class="alert alert-success"><strong>{{$tt}}<strong></strong></td>
                        </tr>
                        <tfoot>
                        </tfoot>
                    </table>
                 
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop
