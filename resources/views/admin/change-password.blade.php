@extends('admin.layout')
@section('title') Update Password @stop
@section('content')

     <div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Change Login Password here</h3>
    </div>
    <!-- /.box-header -->
            <form  method="post" action="{{url('/change-password')}}">
                {{csrf_field()}}
                 <div class="box-body">
                  <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif

                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                        <div class="col-md-6">
                    <!-- /.form-group -->
                    <div class="form-group {{$errors->has('password') ? 'has-danger':''}}">
                        <label>New Password</label>
                        <input type="password" class="form-control " name="password" placeholder="New Password" autofocus="">
                        @if($errors->has('password'))
                            <span class="text-danger">{{$errors->first('password')}}</span>
                        @endif
                    </div>
                    <!-- /.form-group -->

                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label>Repeat New Password</label>
                        <input type="password" class="form-control " name="password_confirmation" placeholder="Confirm New Password">

                    </div>
                    <!-- /.form-group -->

                </div>
                <!-- /.col -->
                </div><!--end row-->
  <div class="row">
                <div class="col-md-6 col-md-offset-5">


                        <button type="submit" class="btn btn-pill-right btn-warning">Change Password</button>
                </div>
</div><!--end row-->
</div><!--end box body-->

    </form>
    </div>
<!-- /.box -->
@stop