
 @extends('admin.layout')
@section('title') Update user Profile @stop
@section('content')

                                <div class="box box-success">
    <div class="box-header with-border">
        <h3 class="box-title">Edit your profile here</h3>
    </div>
    <!-- /.box-header -->
            <form enctype="multipart/form-data" method="post" action="{{url('/admin-profile')}}">
                {{csrf_field()}}
                  <div class="box-body">
                  <div class="row">
                <div class="col-sm-6 col-lg-offset-3">
                  <img src="{{url('users/'.$user->avatar)}}" style="height:400px;width:450px;" class="img-responsive img-circle img-thumbnail">
                </div>
                </div><!--end row-->
                <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                     
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="{{$user->name}}" >
                    </div>
                    <!-- /.form-group -->
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="{{$user->email}}">
                      </div>
                    <!-- /.form-group -->

                </div>
                <!-- /.col -->
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Profile Photo</label>
                        <input type="file" class="form-control " name="avatar" value="{{$user->avatar}}">
                    </div>
                    <!-- /.form-group -->
                 
                    </div> <!-- /.col -->
                    </div><!--end row-->
                    <div class="row">
                <div class="col-md-6 col-md-offset-6">


                        <button type="submit" class="btn btn-pill-right btn-warning" >Update Profile</button>
                    </div>
                    </div><!--end row-->
                    </div><!--end box body-->

                <!-- /.form-group -->
            </form>
              
</div>
<!-- /.box -->

@stop