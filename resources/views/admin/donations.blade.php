@extends('admin.layout')
@section('title') All Donations Made @stop
@section('page') All Donations  @stop
@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">List Showing All Donations Made.</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                      <a href="{{url('/donationsPDF')}}"  class=" btn bg-success pull-right">
        <i class="fa fa-print"></i> Generate PDF</a>
        <h4>&nbsp;</h4>
             <table class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                        
                            <th>Name of Donor</th>
                            <th>Email</th>
                            <th>City/Town</th>
                            <th>Address</th>
                            <th>Payment Type</th>
                            <th>Date Donated</th>
                            <th>Amount</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($donations as $donations)

                            <tr>
                    
                            
                                <td>{{$donations->name}}</td>
                                <td>{{$donations->email}}</td>
                                <td>{{$donations->city}}</td>
                                <td>{{$donations->address}}</td>
                                <td>{{$donations->payment_type}}</td>
                                <td>{{ date('F d, Y', strtotime($donations->created_at)) }}</td>
                                <td>{{$donations->amount}}</td>
                                
                                
        
                            </tr>
                        @endforeach

                        </tbody>
                        <tr>
                            <td colspan="6"><strong>Total</strong></td>
                            <td ><strong>{{$tt}}</strong></td>
                        </tr>
                        <tfoot>
                        </tfoot>
                    </table>
                 
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop
