@extends('admin.layout')
@section('title') New Rehabilitation Center @stop
@section('content')
        <!-- SELECT2 EXAMPLE -->
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Fill in the form below to add a Rehabilitation Center</h3>
    </div>
    <!-- /.box-header -->
    
            <form  enctype="multipart/form-data" method="post" action="{{url('/addrehab')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-6">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('name') ? 'has-error':''}}">
                        <label>Rehab Center Name *</label>
                        <input type="text" name="name" class="form-control" placeholder="eg. watoto wote">
                        @if($errors->has('name'))
              <span class="help-block">{{$errors->first('name')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-6">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('location') ? 'has-error':''}}">
                        <label> Where is it Located?*</label>
                        <input type="text" name="location" class="form-control" placeholder="eg. Kawangware,Nairobi">
                        @if($errors->has('location'))
              <span class="help-block">{{$errors->first('location')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 
                </div><!--end row-->                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-info" >Submit Details</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
   

</div>
<!-- /.box -->
@stop
