@extends('admin.layout')
@section('title') Rehabilitation Center @stop
@section('page') All Rehabilitation Centers @stop
@section('content')
    <div class="row">
        <div class="col-xs-12">

            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Rehabilitation Centers</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                    @endif
                      <a href="{{url('/rehabsPDF')}}"  class=" btn bg-navy pull-right">
        <i class="fa fa-print"></i> Generate PDF</a>
        <h4>&nbsp;</h4>
                    <table id="example1" class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                        
                            <th>Rehabilitation Center Name</th>
                            <th>Location</th>
                            <th>Action</th>

                           
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($rehabs as $rehabs)

                            <tr>
                    
                            
                                <td>{{$rehabs->name}}</td>
                                <td>{{$rehabs->location}}</td>
                                 <td style="width:180px;"><a href="{{route('deleteRehab',['rehabs'=>$rehabs->id])}}"  onclick="return confirm('Do you want to Delete ?')" class="btn btn-danger"><i class="fa fa-fw fa-remove"></i>Delete </a>
                                    <a href="{{url('/viewrehab/'.$rehabs->id)}}" class="btn btn-success"><i class="fa fa-fw fa-eye"></i>View</a></td>
                                
        
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                 
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
@stop
