@extends('admin.layout')
@section('title') New Needy Child @stop
@section('content')
        <!-- SELECT2 EXAMPLE -->
<div class="box box-default">
    <div class="box-header with-border">
        <h3 class="box-title">Fill in the form below to add a new Needy Child</h3>
    </div>
    <!-- /.box-header -->
    
            <form  enctype="multipart/form-data" method="post" action="{{url('/addkid')}}">
                {{csrf_field()}}
                <div class="box-body">
                 <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        @if(Session::has('error'))
                            <div class="alert alert-danger">
                                {{Session::get('error')}}
                            </div>
                        @endif
                        @if(Session::has('success'))
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        @endif
                    </div>
                </div>
                </div><!--end row-->
                <div class="row">
                       <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('fname') ? 'has-error':''}}">
                        <label>First Name *</label>
                        <input type="text" name="fname" class="form-control" placeholder="eg. Jacob">
                        @if($errors->has('fname'))
              <span class="help-block">{{$errors->first('fname')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('lname') ? 'has-error':''}}">
                        <label>Last Name *</label>
                        <input type="text" name="lname" class="form-control" placeholder="eg. Kaime">
                        @if($errors->has('lname'))
              <span class="help-block">{{$errors->first('lname')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 <div class="col-md-4">
                    <div class="form-group {{$errors->has('img') ? 'has-error':''}}">
                        <label>Photo *</label>
                        <input type="file" class="form-control " name="img">
                        @if($errors->has('img'))
              <span class="help-block">{{$errors->first('img')}}</span>
                       @endif
                    </div>
                    </div>
                </div><!--end row-->
               <div class="row">
                <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group">
                        <label>National ID </label>
                        <input type="number" name="nid" class="form-control" placeholder="22334455">
                     
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-md-4">
          <div class="form-group {{$errors->has('gender') ? 'has-error':''}}">
                        <label>Gender*</label>
                     <select class="form-control select2" name="gender">
                     <option>Male</option>
                     <option>Female</option>
                     </select>
             @if($errors->has('gender'))
      <span class="help-block">{{$errors->first('gender')}}</span>
              @endif
                    </div>
        </div><!-- /.col --> 
                <div class="col-md-4">
           <div class="form-group {{$errors->has('dob') ? 'has-error':''}}">
                <label>Date of Birth*</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control" name="dob" id="datepicker" placeholder="mm/dd/yyyy">
                </div>
                <!-- /.input group -->
                  @if($errors->has('dob'))
      <span class="help-block">{{$errors->first('dob')}}</span>
              @endif
              </div>
              <!-- /.form group -->
        </div><!-- /.col -->            
        </div>  <!--end row-->
       
                 <div class="row">
                       <div class="col-md-4">
                <!-- /.form-group -->
                    <div class="form-group {{$errors->has('residence') ? 'has-error':''}}">
                        <label>Place of Residence *</label>
                        <input type="text" name="residence" class="form-control" placeholder="Mathare">
                        @if($errors->has('residence'))
              <span class="help-block">{{$errors->first('residence')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <div class="col-sm-4">
                 <!-- /.form-group -->
                    <div class="form-group {{$errors->has('county') ? 'has-error':''}}">
                        <label>County*</label>
                         <select name="county" class="form-control select2" style="width: 100%;">
                        <option value=""> ---Select County---</option>
                      <option value="Baringo">Baringo</option>
                    <option value="Bomet">Bomet</option>
                    <option value="Bungoma">Bungoma</option>
                    <option value="Busia">Busia</option>
                    <option value="Elgeyo Marakwet">Elgeyo Marakwet</option>
                    <option value="Embu">Embu</option>
                    <option value="Garissa">Garissa</option>
                    <option value="Homabay">Homa Bay</option>
                    <option value="Isiolo">Isiolo</option>
                    <option value="Kajiado">Kajiado</option>
                    <option value="Kakamega">Kakamega</option>
                    <option value="Kericho">Kericho</option>
                    <option value="Kiambu">Kiambu</option>
                    <option value="Kilifi">Kilifi</option>
                    <option value="Kirinyaga">Kirinyaga</option>
                    <option value="Kisii">Kisii</option>
                    <option value="Kisumu">Kisumu</option>
                    <option value="Kitui">Kitui</option>
                    <option value="Kwale">Kwale</option>
                    <option value="Laikipia">Laikipia </option>
                    <option value="Lamu">Lamu</option>
                    <option value="Machakos">Machakos</option>
                    <option value="Makueni">Makueni</option>
                    <option value="Mandera">Mandera</option>
                    <option value="Meru">Meru </option>
                    <option value="Migori">Migori</option>
                    <option value="Marsabit">Marsabit</option>
                    <option value="Mombasa">Mombasa</option>
                    <option value="Muranga">Muranga</option>
                    <option value="Nairobi">Nairobi</option>
                    <option value="Nakuru">Nakuru</option>
                    <option value="Nandi">Nandi</option>
                    <option value="Narok">Narok</option>
                    <option value="Nyamira">Nyamira</option>
                    <option value="Nyandarua">Nyandarua</option>
                    <option value="Nyeri">Nyeri</option>
                    <option value="Samburu">Samburu</option>
                    <option value="Siaya">Siaya</option>
                    <option value="Taita Taveta">Taita Taveta</option>
                    <option value="Tana River">Tana River</option>
                    <option value="Tharaka Nithi">Tharaka Nithi</option>
                    <option value="Trans Nzoia">Trans Nzoia</option>
                    <option value="Turkana">Turkana </option>
                    <option value="Uasin Gishu">Uasin Gishu</option>
                    <option value="Vihiga">Vihiga</option>
                    <option value="Wajir">Wajir</option>
                    <option value="West Pokot">West Pokot</option>
                        </select>
                        @if($errors->has('county'))
              <span class="help-block">{{$errors->first('county')}}</span>
                       @endif
                    </div>
                <!-- /.form-group -->
                </div>
                <!-- /.col -->
                 <div class="col-md-4">
                    <div class="form-group">
                        <label>Education</label>
                        <input type="text" class="form-control " name="education" placeholder="eg. Ongoing">
                      
                    </div>
                    </div>
                </div><!--end row-->
                
                <div class="col-md-6 col-md-offset-5">

                    <div class="form-group">

                        <button type="submit" class="btn btn-info" >Submit Details</button>
                    </div>
                </div>

                <!-- /.form-group -->
                     </div>
        <!-- /.row -->
    </div>
    <!-- /.box-body -->
            </form>
   

</div>
<!-- /.box -->
@stop
