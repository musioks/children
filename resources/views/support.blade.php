<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>NEEDY CHILDREN AND SPONSOR LINKING PLATFORM</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
   <link rel="stylesheet" href="{{asset('bootstrap/css/bootstrap.min.css')}}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('bootstrap/css/font-awesome.min.css')}}">
<!-- DataTables -->
<link rel="stylesheet" href="{{asset('plugins/datatables/dataTables.bootstrap.css')}}">
<!-- jvectormap -->
 <!-- bootstrap datepicker -->
<link rel="stylesheet" href="{{asset('plugins/datepicker/datepicker3.css')}}">
<!-- Select2 -->
<link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css')}}">
<!-- Theme style -->
<link rel="stylesheet" href="{{asset('dist/css/AdminLTE.min.css')}}">
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet" href="{{asset('dist/css/skins/_all-skins.min.css')}}">
    <style type="text/css">
        input[type="email"],input[type="password"],input[type="checkbox"],button[type="submit"]{
            border-radius:5px;
        }
        .jumbotron{
            background: #337ab7;
            color: #FFFFFF;
        }
    </style>
       <script>
    window.Laravel = {!! json_encode([
        'csrfToken' => csrf_token(),
    ]) !!};
</script>
</head>
<body>
<nav class="navbar navbar-inverse" style="border-radius: 0px;">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header page-scroll">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="{{url('/')}}">
					NEEDY CHILDREN AND SPONSOR LINKING PLATFORM
					</a>
				</div>
				<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav navbar-left">
						
						<li>
							<a  href="{{url('/support')}}">Sponsor a Needy Child</a>
						</li>
						<li>
							<a  href="{{url('/donate')}}">Donate</a>
						</li>
					</ul>
						<ul class="nav navbar-nav navbar-right">
						
						<li>
							<a class="" href="{{url('/login')}}">Sign In</a>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container-fluid -->
		</nav>
		<div class="container">
		<div class="jumbotron">
		<h3 class="text-center">SPONSOR A NEEDY CHILD</h3>
		</div>
         <div class="box">
                <div class="box-header">
                    <h3 class="box-title">NEEDY CHILDREN </h3>
                </div>
                            <div class="box-body">

	  <table id="example1" class="table table-bordered table-striped table-condensed">
                        <thead>
                        <tr>
                            <th>Photo</th>
                            <th>Child Name</th>
                            <th>Age</th>
                            <th>Gender</th>
                            <th>Education Progress</th>
                            <th>Residence</th>
                            <th>County</th>
                            <th>Rehab. Center</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($kids as $kids)

                            <tr>
                     <td style="width:200px;"><img src="{{url('kids/'.$kids->img)}}" class="img-responsive"></td>

                                <td>{{$kids->fname}}{{" "}}{{$kids->lname}}</td>
                                <td>
                                @php 
                               
                                  $d=date("m/d/Y");
                                  $d1=$kids->dob;
                                   $date1=new DateTime($d);
                                $date2=new DateTime($d1);
                                $diff = $date1->diff($date2);
                                echo $diff->y;@endphp</td>
                                <td>{{$kids->gender}}</td>
                                <td>{{$kids->education}}</td>
                                <td>{{$kids->residence}}</td>
                                <td>{{$kids->county}}</td>
                                <td>{{$kids->name}}</td>        
                                 <td style="width:180px;">
                            <a href="{{url('/sponsorChild/'.$kids->id)}}" class="btn btn-success"><i class="fa fa-fw fa-fa-handshake-o"></i>Sponsor Child</a></td>       
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                    </div>
                    </div>
		</div>


<script src="{{asset('plugins/jQuery/jquery-2.2.3.min.js')}}"></script>
<!-- Bootstrap 3.3.6 -->
<script src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
<script src="{{asset('plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<!-- FastClick -->
<script src="{{asset('plugins/fastclick/fastclick.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('plugins/select2/select2.full.min.js')}}"></script>
<!-- bootstrap datepicker -->
<script src="{{asset('plugins/datepicker/bootstrap-datepicker.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('dist/js/app.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugins/sparkline/jquery.sparkline.min.js')}}"></script>
<!-- jvectormap -->
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<!-- SlimScroll 1.3.0 -->
<script src="{{asset('plugins/slimScroll/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('plugins/jquery.PrintArea.js')}}"></script>
<!-- ChartJS 1.0.1 -->
<script src="{{asset('plugins/chartjs/Chart.min.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('dist/js/pages/dashboard2.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('dist/js/demo.js')}}"></script>
<script type="text/javascript">
    $('#example1').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
       //Initialize Select2 Elements
$(".select2").select2();
      //Date picker
$('#datepicker').datepicker({
  autoclose: true
});
  $('#datepicker1').datepicker({
  autoclose: true
});
</script>
</body>
</html>