<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('pageNotFound', 'ErrorController@pageNotFound')->name('notFound');
Route::get('/', 'PagesController@index')->name('index');
Route::get('/support', 'PagesController@support')->name('support');
Route::get('/sponsorChild/{id}', 'PagesController@sponsorChild')->name('sponsorChild');
Route::post('/sponsorChild/{id}', 'PagesController@laptop')->name('laptop');
Route::get('/donate', 'PagesController@donate')->name('donate');
Route::post('/donate', 'PagesController@Dnt')->name('Dnt');
Route::get('/create', 'RegisterController@create')->name('create');
Route::post('/create', 'RegisterController@storeAdmin')->name('storeAdmin');
Route::get('/login', 'LoginController@login');
Route::post('/login', 'LoginController@Signin');
Route::post('/signout', 'LoginController@getLogout')->name('signout');


//////////-------------------Admin routes--------------------------////
Route::get('/admin', 'AdminController@index')->name('admin.index');
Route::get('/getUsers', 'AdminController@getUsers')->name('getUsers');
Route::get('/admin-profile','AdminController@admin_profile')->name('profile'); 
Route::post('/admin-profile','AdminController@postProfile')->name('postProfile'); 
Route::get('/change-password','AdminController@change_password')->name('cpassword'); 
Route::post('/change-password','AdminController@passwordChange')->name('passwordChange'); 
Route::get('/deleteUser/{id}', ['uses'=>'AdminController@deleteUser','as'=>'deleteUser']);
//////////-------------------Kid routes--------------------------////
Route::get('/addkid', 'KidController@addkid')->name('addkid');
Route::post('/addkid', 'KidController@storeKid')->name('storeKid');
Route::get('/children', 'KidController@children')->name('children');
Route::get('/viewkid/{id}', 'KidController@editKid')->name('editkid');
Route::post('/viewkid/{id}', 'KidController@updateKid')->name('updatekid');
Route::get('/deleteKid/{id}', 'KidController@deleteKid')->name('deleteKid');
Route::get('/kidsPDF', 'KidController@kidsPDF');
Route::any('/searchScope','KidController@searchScope');
Route::get('/childrenRehab', 'KidController@childrenRehab')->name('childrenRehab');
Route::any('/rehabReport', 'KidController@rehabReport');

//////////-------------------Rehab routes--------------------------////
Route::get('/addrehab', 'RehabController@addrehab')->name('addrehab');
Route::post('/addrehab', 'RehabController@storeRehab')->name('storeRehab');
Route::get('/rehabs', 'RehabController@rehabs')->name('rehabs');
Route::get('/viewrehab/{id}', 'RehabController@editRehab')->name('editRehab');
Route::post('/viewrehab/{id}', 'RehabController@updateRehab')->name('updateRehab');
Route::get('/deleteRehab/{id}', 'RehabController@deleteRehab')->name('deleteRehab');
Route::get('/rehabsPDF', 'RehabController@rehabsPDF');


//////////-------------------Donation routes--------------------------////
Route::get('/donations', 'AdminController@donations')->name('donations');
Route::get('/donationsPDF', 'AdminController@donationsPDF');

//////////-------------------Sponsor routes--------------------------////
Route::get('/sponsors', 'AdminController@sponsors')->name('sponsors');
Route::get('/sponsorsPDF', 'AdminController@sponsorsPDF');

