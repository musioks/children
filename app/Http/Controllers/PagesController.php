<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kid;
use DB;
use Session;
use App\Sponsor;
use App\Donation;
class PagesController extends Controller
{
    //
    public function index(){
        $kids = DB::table('kids')
            ->join('rehabs', 'kids.rehab_id', '=', 'rehabs.id')
            ->select('kids.*', 'rehabs.name')
            ->get();
    	return view('index',['kids'=>$kids]);
    }
     public function support(){
        $kids = DB::table('kids')
            ->join('rehabs', 'kids.rehab_id', '=', 'rehabs.id')
            ->select('kids.*', 'rehabs.name')
            ->get();
        return view('support',['kids'=>$kids]);
    }
     public function sponsorChild($id){
        $kids =Kid::find($id);
        return view('sponsorChild',['kids'=>$kids]);
    }
     public function donate(){
    	return view('donate');
    }
     public function Dnt(Request $request){
            $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'city'=>'required',
            'address'=>'required',
            'amount'=>'required',
            'payment_type'=>'required',
        ]);
            $d=new Donation;
           $d->name=$request->name;
           $d->email=$request->email;
           $d->city=$request->city;
           $d->address=$request->address;
           $d->amount=$request->amount;
           $d->payment_type=$request->payment_type;
           $d->save();
         
       
        return redirect()->route('donate')->with('success','Donation made successfully!');
    }
     public function laptop(Request $request,$id){
            $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'town'=>'required',
            'address'=>'required',
            'amount'=>'required',
            'payment'=>'required',
            'sponsor_period'=>'required',
        ]);
           $l=new Sponsor;
           $l->kid_id=$request->kid_id;
           $l->name=$request->name;
           $l->email=$request->email;
           $l->town=$request->town;
           $l->address=$request->address;
           $l->sponsor_period=$request->sponsor_period;
           $l->amount=$request->amount;
           $l->payment=$request->payment;
           $l->save();
         
       
        return redirect()->back()->with('success','Sponsorship added successfully!');
    }
}
