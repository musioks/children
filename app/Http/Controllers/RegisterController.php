<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Sentinel;
class RegisterController extends Controller
{

public function __construct(){
        $this->middleware('guest');

    }

   public function create(){
    return view('create');
   }

   public function storeAdmin(Request $request){
     $this->validate($request,[
            'name'=>'required',
            'email'=>'required',
            'password'=>'required|confirmed',
        ]);
     $credentials=[
     'name'=>$request->name,
     'email'=>$request->email,
     'password'=>$request->password
     ];
     $user=Sentinel::registerAndActivate($credentials);
     $role=Sentinel::findRoleBySlug('admin');
     $role->users()->attach($user);
     return redirect('/create')->with('success','user has been created!');
   }


}
