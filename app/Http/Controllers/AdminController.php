<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
use App\User;
use App\Kid;
use App\Rehab;
use App\Donation;
use DB;
use PDF;
class AdminController extends Controller
{
    public function __construct(){
        $this->middleware('admin');

    }
    public function getUsers(){
        $users=User::all();
        return view('admin.getUsers',['users'=>$users]);
    }
    public function index(){
    	$users=User::all();
    	$kids=Kid::all();
    	$rehabs=Rehab::all();
    	return view('admin.index',['users'=>$users,'kids'=>$kids,'rehabs'=>$rehabs]);
    }
    public function sponsors(){
        $sponsors = DB::table('sponsors')
            ->join('kids', 'sponsors.kid_id', '=', 'kids.id')
            ->select('sponsors.*', 'kids.fname','kids.lname')
            ->get();
        $tt=DB::table('sponsors')->sum('amount');
    	return view('admin.sponsors',['sponsors'=>$sponsors,'tt'=>$tt]);
    }
    public function sponsorsPDF(){
       $sponsors = DB::table('sponsors')
            ->join('kids', 'sponsors.kid_id', '=', 'kids.id')
            ->select('sponsors.*', 'kids.fname','kids.lname')
            ->get();
        $tt=DB::table('sponsors')->sum('amount');
      $pdf=PDF::loadView('admin.sponsorsPDF',['sponsors'=>$sponsors,'tt'=>$tt])->setPaper('a4', 'landscape');
            return $pdf->stream('sponsors.sponsorsPDF');
   }
    public function donations(){
      $donations=Donation::all();
        $tt=DB::table('donations')->sum('amount');
      return view('admin.donations',['donations'=>$donations,'tt'=>$tt]);
    }
    public function donationsPDF(){
     $donations=Donation::all();
     $tt=DB::table('donations')->sum('amount');
$pdf=PDF::loadView('admin.donationsPDF',['donations'=>$donations,'tt'=>$tt])->setPaper('a4', 'landscape');
            return $pdf->stream('donations.donationsPDF');
   }

  public function admin_profile(){
$user=Sentinel::getUser();
    return view('admin.admin-profile',['user'=>$user]);
  }
   public function postProfile(Request $request){
      $user=Sentinel::getUser();
  if($request->hasFile('avatar')){
       $image=$request->file('avatar');
         $imageName = time().'.'.$image->getClientOriginalExtension();
         $request->avatar->move(public_path('users'), $imageName);
         $user->avatar=$imageName; 
       }
           $user->name=$request->name;
           $user->email=$request->email;
           $user->save();
           return redirect()->back()->with('success','Profile updated.');
}
    public function change_password(){

    return view('admin.change-password');
  }
  public function passwordChange(Request $request){
    $this->validate($request, [
     'password'=>'required|confirmed',
      ]);
    $user=Sentinel::getUser();
    $user->password=bcrypt($request->password);
    $user->save();
    return redirect()->route('cpassword')->with('success','Password Updated.');
  }
      public function deleteUser($id){
        $delete=User::where('id',$id)->first();
        $delete->delete();
        return redirect()->route('getUsers')->with('success','User Deleted Successfully!');
    }
}
