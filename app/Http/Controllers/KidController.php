<?php

namespace App\Http\Controllers;

use App\Kid;
use App\Rehab;
use Session;
use DB;
use PDF;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;

class KidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function children()
    {
         $kids = DB::table('kids')
            ->join('rehabs', 'kids.rehab_id', '=', 'rehabs.id')
            ->select('kids.*', 'rehabs.name')
            ->get();
        
        return view('admin.children',['kids'=>$kids]);
    }
      public function kidsPDF(){
      
    $kids = DB::table('kids')
            ->join('rehabs', 'kids.rehab_id', '=', 'rehabs.id')
            ->select('kids.*', 'rehabs.name')
            ->get();
            $pdf=PDF::loadView('admin.kidsPDF',['kids'=>$kids])->setPaper('a4', 'landscape');
            return $pdf->stream('kids.kidsPDF');
   }
    public function addkid()
    {
        return view('admin.addkid');
    }
    public function storeKid(Request $request)
    {
           $this->validate($request,[
            'fname'=>'required',
            'lname'=>'required',
            'dob'=>'required',
            'gender'=>'required',
            'residence'=>'required',
            'county'=>'required',
            'education'=>'required',
            'img' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:7168',
        ]);
           $rehab=Rehab::first();
            $kid=new Kid;
           if($request->hasFile('img')) {
           $imageName = time().'.'.$request->img->getClientOriginalExtension();
           $request->img->move(public_path('kids'), $imageName);
           $kid->nid=$request->nid;
           $kid->fname=$request->fname;
           $kid->lname=$request->lname;
           $kid->img=$imageName;
           $kid->gender=$request->gender;
           $kid->dob=$request->dob;
           $kid->residence=$request->residence;
           $kid->county=$request->county;
           $kid->education=$request->education;
           $kid->rehab_id=$rehab->id;
           $kid->save();
         }
       
        return redirect()->route('addkid')->with('success','Student created successfully!');
    }
    public function editKid($id)
    {
        $kids=Kid::find($id);
        $rehabs=DB::table('rehabs')->get();
        return view('admin.viewkid',['kids'=>$kids,'rehabs'=>$rehabs]);
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Kid  $kid
     * @return \Illuminate\Http\Response
     */
    public function updateKid(Request $request,$id)
    {
            $kid=Kid::find($id);
            $kid->fname=$request->fname;
           $kid->lname=$request->lname;
           if($request->hasFile('img')) {
           $imageName = time().'.'.$request->img->getClientOriginalExtension();
           $request->img->move(public_path('kids'), $imageName);
           $kid->img=$imageName;
            }
           $kid->nid=$request->nid;
           $kid->gender=$request->gender;
           $kid->dob=$request->dob;
           $kid->residence=$request->residence;
           $kid->county=$request->county;
           $kid->education=$request->education;
           $kid->rehab_id=$request->rehab_id;
           $kid->save();
       
        return redirect()->route('children')->with('success','Details Updated successfully!');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Kid  $kid
     * @return \Illuminate\Http\Response
     */
    public function deleteKid($id)
    {
        $delete=Kid::find($id);
        $delete->delete();
        return redirect()->route('children')->with('success','Child Deleted Successfully!');
        //
    }
    public function childrenRehab()
    {
        $rehabs=Rehab::all();
        return view('admin.childrenRehab',['rehabs'=>$rehabs]);
}
public function searchScope(){
    $rehabs=Rehab::all();
  $q = Input::get( 'rehab_id' );
    $kids= Kid::where ( 'rehab_id', 'LIKE', '%' . $q . '%' )->get();
    if (count ( $kids ) > 0)
        return view ( 'admin.childrenRehab',['rehabs'=>$rehabs])->withDetails( $kids )->withQuery ( $q );
    else
    return view ( 'admin.childrenRehab',['rehabs'=>$rehabs])->withMessage( 'No Details found. Try to search again !' );
} 
public function rehabReport(){
        $q = Input::get( 'rehab_id' );
        $kids= DB::table('kids')->where( 'rehab_id','=',$q )->get();
            $pdf=PDF::loadView('admin.rehabReport',['kids'=>$kids]);
            return $pdf->stream('rehabReport.rehabReport');
   }
}
