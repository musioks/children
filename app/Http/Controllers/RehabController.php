<?php

namespace App\Http\Controllers;

use App\Rehab;
use PDF;
use Illuminate\Http\Request;

class RehabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function rehabs()
    {
       $rehabs=Rehab::all();
        return view('admin.rehabs',['rehabs'=>$rehabs]);
    }
    public function addrehab()
    {
         return view('admin.addrehab');
    }
       public function rehabsPDF(){
     $rehabs=Rehab::all();
            $pdf=PDF::loadView('admin.rehabsPDF',['rehabs'=>$rehabs]);
            return $pdf->stream('rehabs.rehabsPDF');
   }

    public function storeRehab(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'location'=>'required',
       
        ]);
            $rehab=new Rehab;
         
           $rehab->name=$request->name;
           $rehab->location=$request->location;
           $rehab->save();
       
        return redirect()->route('addrehab')->with('success','Rehab Center created successfully!');
        
    }

    public function editRehab($id)
    {
        $rehabs=Rehab::find($id);
        return view('admin.viewrehab',['rehabs'=>$rehabs]);
    }

  
    public function updateRehab(Request $request,$id)
    {
            $rehab=Rehab::find($id);
           $rehab->name=$request->name;
           $rehab->location=$request->location;
           $rehab->save();
       
        return redirect()->route('rehabs')->with('success','Details Updated successfully!');
    }

    
    public function deleteRehab($id)
    {
         $delete=Rehab::find($id);
        $delete->delete();
        return redirect()->route('rehabs')->with('success','Rehab center Deleted Successfully!');
    }
}
