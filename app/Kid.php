<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kid extends Model
{
     protected $fillable = [
        'fname','lname','nid','residence','county','gender','dob',
    ];
}
