<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rehab extends Model
{
     protected $fillable = [
        'name', 'location',
    ];
}
